package luxoft.project.steps;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import utils.BaseUtils;

public class Hook extends BaseUtils {

    private BaseUtils base;

    public Hook(BaseUtils base) {
        this.base = base;
    }

    @Before
    public void startOfScenario(Scenario scenario){
//        System.setProperty("webdriver.chrome.driver","C:\\project_047\\newproject047\\src\\test\\resources\\drivers\\chromedriver.exe");
//        base.driver = new ChromeDriver();

        System.setProperty("webdriver.ie.driver","src/test/resources/drivers/IEDriverServer.exe");
        base.driver = new InternetExplorerDriver();
        base.driver.manage().window().maximize();
        //System.out.println("Scenario with name" + scenario.getName() + " is started");
    }


    @After
    public void endOfScenario(Scenario scenario) throws Exception{
       base.driver.close();
        //System.out.println("Scenario with tag"+ scenario.getSourceTagNames() + " is ended");
//       if (scenario.isFailed() == true){
//           System.out.println("Failed");
//               }
//
}
}
