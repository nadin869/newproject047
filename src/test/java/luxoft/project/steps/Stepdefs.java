package luxoft.project.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import utils.BaseUtils;

public class Stepdefs extends BaseUtils{
    //    private List<People> people ;
//
//
//    public static class People {
//        public String name, surname, sex;
//        public int age;
//        public boolean white;
//        public long password;
//    }
//
//    @Given("^information abot person$")
//           public void informationAbotPerson(@Transpose List<People> peopleLists) throws Throwable {
//            for (People peopleList : peopleLists) {
//               // people.add(peopleList.name, peopleList.surname, peopleList.sex, peopleList.white, peopleList.age, peopleList.password);
//                System.out.println(peopleList);
//            }
//    }


        /*@Given("^I have (\\d+) cukes in my belly$")
        public void I_have_cukes_in_my_belly(int cukes) throws Throwable {
            Belly belly = new Belly();
            belly.eat(cukes);
        }

        @When("^I wait (\\d+) hour$")
        public void iWaitHour(int arg0) throws Throwable {
            // Write code here that turns the phrase above into concrete actions
            System.out.println("I wait " + arg0);
        }

        @Then("^my belly should growl$")
        public void myBellyShouldGrowl() throws Throwable {
            // Write code here that turns the phrase above into concrete actions
            System.out.println("Success");
        }

        @When("^i see (\\d*) and see (\\d*)$")
        public void iSeeFirstDigitAndSeeSecondDigit(int a, int b) throws Throwable {
            System.out.println(a + b);
        }

        @When("^Rectangular triangle has relation sides (\\d+), (\\d+), (\\d+)$")
        public void rectangularTriangleHasRelationABC(int arg0, int arg1, int arg2) throws Throwable {
            if (sqrt(arg0) + sqrt(arg1) == sqrt(arg2)) {
                System.out.println("Yes - rectangularTriangle");

            } else {
                System.out.println("No - rectangularTriangle");
            }
        }

        @Given("^Ihave one interested table:$")
        public void ihaveOneInterestedTable(DataTable datatable) throws Throwable {
            List<List<String>> someList = new ArrayList<>();
            someList= datatable.raw();
            System.out.println(someList.get(0).get(1).toString());
        }
    */
        private BaseUtils base;
        public Stepdefs(BaseUtils base) {
            this.base = base;
        }

        @Given("^I login to the system$")
        public void iLoginToTheSystem() throws Throwable {
            System.out.println("Hello, Selenium");
            base.driver.get("http://shipovalov.net");
        }

        @When("^I enter username \"([^\"]*)\" and password \"([^\"]*)\"$")

        public void iEnterAnd(String username, String password) throws Throwable {
            base.driver.findElement(By.name("username")).clear();
            base.driver.findElement(By.name("username")).sendKeys(username);
            base.driver.findElement(By.name("password")).clear();
            base.driver.findElement(By.name("password")).sendKeys(password);
            base.driver.findElement(By.cssSelector("input.button"));
        }
    }

